﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {   
        private Parking _parking;
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;

        public ILogService LogService
        {
            get { return _logService; }
            set { _logService = value; }
        }

        public ITimerService LogTimer
        {
            get { return _logTimer; }
            set { _logTimer = value; }
        }

        public ITimerService WithdrawTimer
        {
            get { return _withdrawTimer; }
            set { _withdrawTimer = value; }
        }

        public Parking Parking
        {
            get { return _parking; }
            set { _parking = value; }
        }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
            Parking = new Parking(0);
        }

        public ParkingService()
        {
            Parking = new Parking(0);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            foreach (var item in Parking.Vehicles)
            {
                if (item.Id == vehicle.Id)
                {
                    return;
                }
            }
            Parking.Vehicles.Add(vehicle);
            System.Console.WriteLine("Add vehicle");
        }

        public void Dispose()
        {
            
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            throw new System.NotImplementedException();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new System.NotImplementedException();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var readOnlyList = new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
            return readOnlyList;
        }

        public string ReadFromLog()
        {
            throw new System.NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            foreach (var item in Parking.Vehicles)
            {
                if (item.Id == vehicleId)
                {
                    Parking.Vehicles.Remove(item);
                    break;
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            foreach (var item in Parking.Vehicles)
            {
                if (item.Id == vehicleId)
                {
                    item.Balance += sum;
                    break;
                }
            }
        }
    }
}