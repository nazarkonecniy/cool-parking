﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        private DateTime _time;
        private string _vehicleId;
        private decimal _sum;

        public decimal Sum
        {
            get { return _sum; }
            set { _sum = value; }
        }


        public string VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

    }
}