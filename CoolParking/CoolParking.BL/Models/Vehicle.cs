﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the    tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string _id;
        private VehicleType _vehicleType;
        private decimal _balance;
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            private set { _vehicleType = value; }
        }

        public string Id
        {
            get { return _id; }
            private set { _id = value; }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string number = "";
            char letter;
            Random rand = new Random();

            for (int i = 0; i < 2; i++)
            {
                letter = (char)(rand.Next(65, 90));
                number += letter;
            }

            number += "-";
            int numb = rand.Next(1000, 10000);
            number += numb.ToString();

            number += "-";
            for (int i = 0; i < 2; i++)
            {
                letter = (char)(rand.Next(65, 90));
                number += letter;
            }

            return number;
        }
    }
}