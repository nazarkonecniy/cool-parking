﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private decimal _balance;
        private List<Vehicle> _vehicles;
        private static Parking instance;

        public Parking(decimal balance)
        {
            Balance = balance;
        }

        public List<Vehicle> Vehicles
        {
            get { return _vehicles; }
            set { _vehicles = value; }
        }

        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        public static Parking getInstance(decimal balance)
        {
            if (instance == null)
                instance = new Parking(balance);
            return instance;
        }
    }
}